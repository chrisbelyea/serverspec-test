require 'serverspec'

set :backend, :exec

# elementaryos isn't supported, so act like ubuntu
set :os, :family => 'ubuntu', :release => '14.04', :arch => 'x86_64'
puts "OS Family:  #{os[:family]}"
puts "OS Release: #{os[:release]}"
